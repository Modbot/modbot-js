# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.5.1] - 2019-03-08

### Fixed

- Fixed joint analog input value parsing


## [0.5.0] - 2019-02-07

### Added

- Added `getTargetPositionStat:qe` method
- Added `setRelativeTargetPose` method
- Added `setRelativeTargetPosition` method

### Changed

- Changed `runRobot` to `setRobotRun` to adhere to conventions
- Changed `getPositionState` to `getActualPositionState` to adhere to conventions
- Changed `setPositionState` to `setTargetPositionState` to adhere to conventions
- Changed `getVelocityState` to `getTargetVelocityState` to adhere to conventions
- Changed `setVelocityState` to `setTargetVelocityState` to adhere to conventions


## [0.4.0] - 2018-1-29

### Added

- Added `getSequenceIsRunning` method
- Added `getSequencerStatus` method
- Added `haltSequence` method
- Added `runSequence` method

### Fixed

- Fixed ws endpoint string checking


## [0.3.0] - 2018-12-1

### Added

- Added `getAnalogInputs` method
- Added `getDigitalInputs` method
- Added `getJointAnalogInputs` method
- Added `getJointDigitalInputs` method
- Added `setDigitalOutputs` method
- Added `setJointDigitalOutputs` method

### Changed

- Changed JSMQ dependency to `develop` branch
- Changed boolean parsing in API calls to use `JSMQ.popBoolean`, etc


## [0.2.0] - 2018-12-05

### Added

- Added `setRobotControlMode` method

### Fix

- Fix `getJointBrake` and `setJointBrake` marshalling and unmarshalling

## [0.1.0] - 2018-11-28

### Added

- Added node modules
- Added tests
- Add API calls for each of the modbot ZMQ service methods (of "motion-api")
- Add onConnect / onDisconnect callbacks