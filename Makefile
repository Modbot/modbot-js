.PHONY: all build clean install-dependencies install-system-dependencies run dev dev-run


# Build Configuration
OS:=$(shell uname -s)
PROJECT_NAME:="Modbot-JS"

CMD_INSTALL_SYSTEM_DEPENDENCIES = @echo "Install command undefined. Are you on Ubuntu or Mac OSX?"

ifeq ($(OS),Linux)
	CMD_INSTALL_SYSTEM_DEPENDENCIES = curl -sL https://deb.nodesource.com/setup_8.x | sudo bash - && sudo apt-get install -y nodejs && echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf

else ifeq ($(OS),Darwin)
	CMD_INSTALL_SYSTEM_DEPENDENCIES = brew install node

endif


all: clean install-dependencies build run

clean:
	npm prune

install-dependencies:
	npm install

install-system-dependencies:
	$(CMD_INSTALL_SYSTEM_DEPENDENCIES)

test: test-offline test-live

test-offline:
	@echo "Beginning offline tests for $(PROJECT_NAME)"
	npm test

test-live:
	@echo "Beginning live tests for $(PROJECT_NAME)"
	./node_modules/jest/bin/jest.js --runInBand --verbose test/modbot-v1/modbot-live.test.js

