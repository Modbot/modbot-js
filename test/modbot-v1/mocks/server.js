import { Server as MockServer } from 'mock-socket';
import { Message, NumberUtility } from '@modbot/jsmq';
import { ServoControlMode } from '../../../src/modbot-v1/modbot';
import {
  messageToFloatArray,
  messageToPose,
} from '../../../src/modbot-v1/utility';
import * as Command from '../../../src/modbot-v1/commands.js';

export class Server extends MockServer {
  constructor(address){
    super(address);

    this._incomingMessage = null;
    this._socket = null;
    this.stage = {};

    this.listRobots = jest.fn( () => {
      let res = new Message();
      res.addInt(Object.keys(this.stage.robots).length);
      for (var key in this.stage.robots) {
        // check if the property/key is defined in the object itself, not in parent
        if (this.stage.robots.hasOwnProperty(key)) {           
          res.addInt(this.stage.robots[key].id);
          res.addString(this.stage.robots[key].name);
        }
      }
      return res;
    });

    this.getActualPose = jest.fn((robotId) => {
      if (robotId in this.stage.robots) {
        let res = new Message();
        let pose = this.stage.robots[robotId].poseActual;
        res.addBuffer(NumberUtility.floatArrayToBytes([pose.x, pose.y, pose.z], 8));
        res.addBuffer(NumberUtility.floatArrayToBytes([pose.qw, pose.qx, pose.qy, pose.qz], 8));
        return res;
      } else {
        return null;
      }
    });

    this.getJointActual = jest.fn( (robotId, jointId, controlMode) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          switch (controlMode) {
            case ServoControlMode.POSITION:
              return new Message().addFloat(this.stage.robots[robotId].servos[jointId].positionActual);
            case ServoControlMode.VELOCITY:
              return new Message().addFloat(this.stage.robots[robotId].servos[jointId].velocityActual);
            case ServoControlMode.TORQUE:
              return new Message().addFloat(this.stage.robots[robotId].servos[jointId].torqueActual);
            default:
              return null;
          }
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.getJointBrake = jest.fn( (robotId, jointId) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          let res = new Message();
          res.addInt(this.stage.robots[robotId].servos[jointId].brake, 1);
          return res;
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.getJointCount = jest.fn( (robotId) => {
      if (robotId in this.stage.robots) {
        if ("servos" in this.stage.robots[robotId]) {
          return new Message().addInt(Object.keys(this.stage.robots[robotId].servos).length);
        } else {
          return new Message().addInt(0);
        }
      } else {
        return null;
      }
    });

    this.getJointControlMode = jest.fn( (robotId, jointId) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          let res = new Message();
          res.addInt(this.stage.robots[robotId].servos[jointId].controlMode);
          return res;
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.getJointTarget = jest.fn( (robotId, jointId, controlMode) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          switch (controlMode) {
            case ServoControlMode.POSITION:
              return new Message().addFloat(this.stage.robots[robotId].servos[jointId].positionTarget);
            case ServoControlMode.VELOCITY:
              return new Message().addFloat(this.stage.robots[robotId].servos[jointId].velocityTarget);
            case ServoControlMode.TORQUE:
              return new Message().addFloat(this.stage.robots[robotId].servos[jointId].torqueTarget);
            default:
              return null;
          }
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.getActualPositionState = jest.fn( (robotId) => {
      if (robotId in this.stage.robots) {
        let res = new Message();
        let state = this.stage.robots[robotId].positionStateActual;
        res.addUint(state.length);
        res.addBuffer(NumberUtility.floatArrayToBytes(state, 8));
        return res;
      } else {
        return null;
      }
    });

    this.getTargetPose = jest.fn( (robotId) => {
      if (robotId in this.stage.robots) {
        let res = new Message();
        let pose = this.stage.robots[robotId].poseTarget;
        res.addBuffer(NumberUtility.floatArrayToBytes([pose.x, pose.y, pose.z], 8));
        res.addBuffer(NumberUtility.floatArrayToBytes([pose.qw, pose.qx, pose.qy, pose.qz], 8));
        return res;
      } else {
        return null;
      }
    });

    this.getTargetVelocityState = jest.fn( (robotId) => {
      if (robotId in this.stage.robots) {
        let res = new Message();
        let state = this.stage.robots[robotId].positionStateActual;
        res.addInt(state.length);
        for (var i in state) {
          res.addFloat(state[i]);
        }
        return res;
      } else {
        return null;
      }
    });

    // Set

    this.setJointBrake = jest.fn( (robotId, jointId, brake) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          this.stage.robots[robotId].servos.brake = brake;
          return new Message();
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.setJointControlMode = jest.fn( (robotId, jointId, controlMode) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          this.stage.robots[robotId].servos.controlMode = controlMode;
          return new Message();
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.setJointTarget = jest.fn( (robotId, jointId, controlMode, target) => {
      if (robotId in this.stage.robots) {
        if (jointId in this.stage.robots[robotId].servos) {
          switch (controlMode) {
            case ServoControlMode.POSITION:
              this.stage.robots[robotId].servos.positionTarget = target;
              break;
            case ServoControlMode.VELOCITY:
              this.stage.robots[robotId].servos.velocityTarget = target;
              break;
            case ServoControlMode.TORQUE:
              this.stage.robots[robotId].servos.torqueTarget = target;
              break;
            default:
              return null;
            }
          return new Message();
        } else {
          return null;
        }
      } else {
        return null;
      }
    });

    this.setTargetPositionState = jest.fn( (robotId, state) => {
      if (robotId in this.stage.robots) {
        this.stage.robots[robotId].positionStateTarget = state;
        return new Message();
      } else {
        return null;
      }
    });
    
    this.setTargetPose = jest.fn( (robotId, pose) => {
      if (robotId in this.stage.robots) {
        this.stage.robots[robotId].poseTarget = pose;
        return new Message();
      } else {
        return null;
      }
    });

    
    this.on('connection', socket => {
      this._socket = socket;
      socket.on('message', message => {
        this._webSocketOnMessage(message);
      });
    });
  }

  handleMessage(message) {
    let response = null;
    let service = message.popString();
    let command = message.popInt(2);
    
    switch (service) {
      case "discovery":
        response = this._serviceDiscovery(command, message);
        break;
        case "motion":
        response = this._serviceMotion(command, message);
        break;
      default:
    }

    if (response == null) {
      response = new Message().addInt(-1).addString("Mock failure");
    } else {
      response.insertInt(0, command, 2);
    }
    this._write(response);
  }
  
  
  setup(stage) {
    this.stage = stage;
  }
  
  _serverOnMessage() {
    this.handleMessage(this._incomingMessage);
    this._incomingMessage = null;
  }
  
  _processFrame(frame) {
    const view = new Uint8Array(frame);
    const more = view[0];
    const payload = new Uint8Array(view.subarray(1));
    
    if (this._incomingMessage == null) {
      this._incomingMessage = new Message();
    }
    
    // Add buffer to the incoming message, removing the MORE byte
    this._incomingMessage.addBuffer(payload);
    
    // Last message frame, ZeroMQ message is complete
    if (more == 0) {
      this._serverOnMessage();
    }
  }
  
  _serviceDiscovery(command, message) {
    switch (command) {
      case Command.LIST_ROBOTS:
        return this.listRobots();
        break;
    }
  }
  
  _serviceMotion(command, message) {
    let robotId = message.popUint();
    let jointId = -1;
    let controlMode = -1;
    switch (command) {
      case Command.GET_ACTUAL_POSE:
        return this.getActualPose(robotId);

      case Command.GET_JOINT_ACTUAL:
        jointId = message.popUint();
        controlMode = message.popUint();
        return this.getJointActual(robotId, jointId, controlMode);
        
      case Command.GET_JOINT_BRAKE:
        jointId = message.popUint();
        return this.getJointBrake(robotId, jointId);

      case Command.GET_JOINT_COUNT:
        return this.getJointCount(robotId);
        
      case Command.GET_JOINT_CONTROL_MODE:
        jointId = message.popUint();
        return this.getJointControlMode(robotId, jointId);
  
      case Command.GET_JOINT_TARGET:
        jointId = message.popUint();
        controlMode = message.popInt(2);
        return this.getJointTarget(robotId, jointId, controlMode);
        
      case Command.GET_ACTUAL_POSITION_STATE:
        return this.getActualPositionState(robotId);
        
      case Command.GET_TARGET_POSE:
        return this.getTargetPose(robotId);
        
      case Command.GET_TARGET_VELOCITY_STATE:
        return this.getTargetVelocityState(robotId);


      case Command.SET_JOINT_BRAKE:
        jointId = message.popUint();
        let brake = message.popInt(1) > 0 ? true : false;
        return this.setJointBrake(robotId, jointId, brake);

      case Command.SET_JOINT_CONTROL_MODE:
        jointId = message.popUint();
        controlMode = message.popUint();
        return this.setJointControlMode(robotId, jointId, controlMode);
  
      case Command.SET_JOINT_TARGET:
        jointId = message.popUint();
        controlMode = message.popInt(2);
        let target = message.popFloat();
        return this.setJointTarget(robotId, jointId, controlMode, target);
        
      case Command.SET_TARGET_POSITION_STATE:
        let positionState = [];
        let count = message.popUint();
        for (let i = 0; i < count; i++) {
          positionState.push(message.popFloat());
        }
        let vel = message.popFloat;
        return this.setTargetPositionState(robotId, positionState, vel);
        
      case Command.SET_TARGET_POSE:
        let targetPose = messageToPose(message);
        return this.setTargetPose(robotId, targetPose);
        
      case Command.SET_TARGET_VELOCITY_STATE:
        let velocityState = messageToFloatArray(message);
        return this.setTargetVelocityState(robotId, velocityState);
    }
  }

  _webSocketOnMessage(e) {
    this._processFrame(e.buffer);
  }

  _write(message) {
    const messageSize = message.getSize();

    for (let j = 0; j < messageSize; j++) {
      const frame = message.getBuffer(j);

      let data = new Uint8Array(frame.byteLength + 1);
      let more = j == messageSize - 1 ? 0 : 1;
      data[0] = more;  // set the MORE byte
      data.set(new Uint8Array(frame), 1);

      this._socket.send(data);
    }
  }
}