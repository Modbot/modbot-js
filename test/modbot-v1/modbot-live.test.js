global.WebSocket = WebSocket;
window.WebSocket = WebSocket;

import { Client, ServoControlMode } from '../..'

const hostAddress = "ws://127.0.0.1:15798";
const testRobotId = 0;
const testJointId = 1;

const jointParameters = [
  { label: "kpp",                        value: "10" },
  { label: "kpi",                        value: "11" },
  { label: "kpd",                        value: "12" },
  { label: "kvp",                        value: "1" },
  { label: "kvi",                        value: "2" },
  { label: "kvd",                        value: "3" },
  { label: "max_torque",                 value: "1200" },
  { label: "position_control_strategy",  value: "2" },
];


describe('client connection', () => {
  var client;

  beforeAll(() => {
    console.log("TESTING - client connection");
  });

  afterAll((done) => {
    client.onDisconnect = () => {
      done();
    };
    client.disconnect();
  });

  test('client connection success', (done) => {
    let clientOnReady = () => {
      done();
    };

    client = new Client(hostAddress);
    client.onConnect = clientOnReady;
    client.connect();
  });
});

describe('robot get commands', () => {
  var client;

  beforeAll((done) => {
    console.log("TESTING - robot get commands");
    client = new Client(hostAddress);
    client.onConnect = () => {
      done();
    }
    client.connect();
  })

  afterAll((done) => {
    client.onDisconnect = () => {
      done();
    };
    client.disconnect();
  });

  test('list robots', async (done) => {

    const res = await client.listRobots();
    expect(res).toBeDefined();

    console.log("List robots:", res.payload);
    done();
  });

  test('get robot actual pose', async (done) => {

    const res = await client.getActualPose(testRobotId);
    expect(res).toBeDefined();

    console.log("Get actual pose [" + testRobotId + "]:", res.payload);
    done();
  });

  test('get robot joint count', async (done) => {

    const res = await client.getJointCount(testRobotId);
    expect(res).toBeDefined();

    console.log("Get joint count [" + testRobotId + "]:", res.payload);
    done();
  });

  test('get robot actual position state', async (done) => {

    const res = await client.getActualPositionState(testRobotId);
    expect(res).toBeDefined();

    console.log("Get actual position state [" + testRobotId + "]:", res.payload);
    done();
  });

  test('get robot target pose', async (done) => {

    const res = await client.getTargetPose(testRobotId);
    expect(res).toBeDefined();

    console.log("Get target pose [" + testRobotId + "]:", res.payload);
    done();
  });
});

describe('robot set commands', () => {
  var client;

  beforeAll((done) => {
    console.log("TESTING - robot set commands");
    client = new Client(hostAddress);
    client.onConnect = () => {
      done();
    }
    client.connect();
  })

  afterAll((done) => {
    client.onDisconnect = () => {
      done();
    };
    client.disconnect();
  });

  test('set robot target position state', async (done) => {
    let state = [0.01, -0.02, -0.03, 0.04, 0.05];
    const res = await client.setTargetPositionState(testRobotId, state, 30);
    expect(res).toBeTruthy();

    console.log("Set target position state [" + testRobotId + "] to", state);
    done();
  });

  test('set robot target pose', async (done) => {
    let pose = {
      x: 0.01,
      y: 0.02,
      z: 0.03,
      qw: 0.707,
      qx: 0.0,
      qy: 0.707,
      qz: 0.0,
    }
    const res = await client.setTargetPose(testRobotId, pose);
    expect(res).toBeTruthy();

    console.log("Set target pose [" + testRobotId + "] to", pose);
    done();
  });
});

describe('joint get commands', () => {
  var client;

  beforeAll((done) => {
    console.log("TESTING - joint get commands");
    client = new Client(hostAddress);
    client.onConnect = () => {
      done();
    }
    client.connect();
  })

  afterAll((done) => {
    client.onDisconnect = () => {
      done();
    };
    client.disconnect();
  });

  test('get joint actual (position)', async (done) => {

    const res = await client.getJointActual(testRobotId, testJointId, ServoControlMode.POSITION);
    expect(res).toBeDefined();

    console.log("Get joint actual (position) [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint actual (torque)', async (done) => {

    const res = await client.getJointActual(testRobotId, testJointId, ServoControlMode.VELOCITY);
    expect(res).toBeDefined();

    console.log("Get joint actual (torque) [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint actual (velocity)', async (done) => {

    const res = await client.getJointActual(testRobotId, testJointId, ServoControlMode.TORQUE);
    expect(res).toBeDefined();

    console.log("Get joint actual (velocity) [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint brake', async (done) => {

    const res = await client.getJointBrake(testRobotId, testJointId);
    expect(res).toBeDefined;

    console.log("Get joint brake [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint control mode', async (done) => {

    const res = await client.getJointControlMode(testRobotId, testJointId);
    expect(res).toBeDefined();

    console.log("Get joint control mode [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint parameter', async (done) => {

    for (let i  = 0; i < jointParameters.length; i++) {
      const res = await client.getJointParameter(testRobotId, testJointId, jointParameters[i].label);
      expect(res).toBeDefined();

      console.log("Get joint parameter [" + testRobotId + "][" + testJointId + "]["
        + jointParameters[i].label + "]", res.payload);
      done();
    }
  });

  test('get joint target (position)', async (done) => {

    const res = await client.getJointTarget(testRobotId, testJointId, ServoControlMode.POSITION);
    expect(res).toBeDefined();

    console.log("Get joint target (position) [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint target (torque)', async (done) => {

    const res = await client.getJointTarget(testRobotId, testJointId, ServoControlMode.VELOCITY);
    expect(res).toBeDefined();

    console.log("Get joint target (torque) [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });

  test('get joint target (velocity)', async (done) => {

    const res = await client.getJointTarget(testRobotId, testJointId, ServoControlMode.TORQUE);
    expect(res).toBeDefined();

    console.log("Get joint target (velocity) [" + testRobotId + "][" + testJointId + "]:", res.payload);
    done();
  });
});

describe('joint set commands', () => {
  var client;

  beforeAll((done) => {
    console.log("TESTING - joint set commands");
    client = new Client(hostAddress);
    client.onConnect = () => {
      console.log(" - beforeAll done()");
      done();
    }
    client.connect();
  })

  afterAll((done) => {
    client.onDisconnect = () => {
      console.log(" - afterAll done()");
      done();
    };
    client.disconnect();
  });


  test('set joint brake', async (done) => {

    let res = await client.setJointBrake(testRobotId, testJointId, true);
    expect(res).toBeTruthy();

    console.log("Set joint brake [" + testRobotId + "][" + testJointId + "] to", true);
    done();
  });

  test('set joint control mode', async (done) => {

    let mode = ServoControlMode.TORQUE;
    const res = await client.setJointControlMode(testRobotId, testJointId, mode);
    expect(res).toBeTruthy();

    console.log("Set joint control mode [" + testRobotId + "][" + testJointId + "] to", mode);
    done();
  });

  test('set joint parameter', async (done) => {

    for (let i  = 0; i < jointParameters.length; i++) {
      let label = jointParameters[i].label;
      let value = jointParameters[i].value;
      const res = await client.setJointParameter(testRobotId, testJointId, label, value);
      expect(res).toBeTruthy();

      console.log("Set joint parameter [" + testRobotId + "][" + testJointId + "]["
        + label + "] to", value);
        done();
    }
  });

  test('set joint target (position)', async (done) => {

    let target = -1.2345
    const res = await client.setJointTarget(testRobotId, testJointId, ServoControlMode.POSITION, target);
    expect(res).toBeTruthy();

    console.log("Set joint target (position) [" + testRobotId + "][" + testJointId + "] to", target);
    done();
  });

  test('set joint target (torque)', async (done) => {

    let target = -1.2345
    const res = await client.setJointTarget(testRobotId, testJointId, ServoControlMode.VELOCITY, target);
    expect(res).toBeTruthy();

    console.log("Set joint target (torque) [" + testRobotId + "][" + testJointId + "] to", target);
    done();
  });

  test('set joint target (velocity)', async (done) => {

    let target = -1.2345
    const res = await client.setJointTarget(testRobotId, testJointId, ServoControlMode.TORQUE, target);
    expect(res).toBeTruthy();

    console.log("Set joint target (velocity) [" + testRobotId + "][" + testJointId + "] to", target);
    done();
  });

});