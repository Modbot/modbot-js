import { WebSocket } from 'mock-socket';
import { Server } from './mocks/server.js';
global.WebSocket = WebSocket;
window.WebSocket = WebSocket;

import { Client, ServoControlMode } from '../..'

const addressA = "ws://localhost:8080";
const addressB = "ws://fakeaddress:1234";

var testPose = { x: 1.1, y: 2.2, z: 3.3, qw: 1.000, qx: 0.0, qy: 0.0, qz: 0.0 };
var testPositionState = [ 444.444, -555.555, 666.666 ];
var stage = {
  robots: {
    // Robot 0
    0: {
      id: 0,
      name: "robot0",
      poseActual: { x: 1.2, y: 3.4, z: 5.6, qw: 0.707, qx: 0.0, qy: 0.707, qz: 0.0 },
      poseTarget: { x: 8.7, y: 6.5, z: 4.3, qw: 0.0, qx: 0.707, qy: 0.0, qz: 0.707 },
      positionStateActual: [ 45.67, -234.567, 1001 ],
      positionStateTarget: [ 50.05, -300, 1000 ],
      servos: {
        0: {
          brake: false,
          controlMode: 2,
          positionActual: 12.34,
          positionTarget: 56.78,
          torqueActual: 111,
          torqueTarget: 222,
          velocityActual: -87.65,
          velocityTarget: -43.21,
        },
        1: {
          brake: false,
          controlMode: 2,
          positionActual: -1234.5678,
          positionTarget: 8765.4321,
          torqueActual: -333,
          torqueTarget: -444,
          velocityActual: 1.234,
          velocityTarget: 5.678,
        },
        2: {
          brake: false,
          controlMode: 2,
          positionActual: 1111,
          positionTarget: -9999,
          torqueActual: 555,
          torqueTarget: -666,
          velocityActual: -1111,
          velocityTarget: 9999,
        },
      },
    },
    // Robot 5
    5: {
      id: 5,
      name: "robot5",
      poseActual: {
        x: NaN, y: NaN, z: NaN,
        qw: NaN, qx: NaN, qy: NaN, qz: NaN
      },
      poseTarget: {
        x: NaN, y: NaN, z: NaN,
        qw: NaN, qx: NaN, qy: NaN, qz: NaN
      },
      positionStateActual: [ 95 ],
      positionStateTarget: [ 100 ],
      servos: {
        0: {
          brake: true,
          controlMode: 3,
          positionActual: 99.99999,
          positionTarget: 100,
          torqueActual: 0,
          torqueTarget: 0,
          velocityActual: 0,
          velocityTarget: 0,
        },
      }
    },
  }
};

beforeAll(() => {
});


describe('client connection', () => {
  var client;
  var server;

  afterEach(() => {
    try {
      client.disconnect();
    } catch (e) {
      // Do nothing
    }
    server.close();
    server = null;
    client = null;
  });
  beforeEach(() => {
    server = new Server(addressA);
  });

  test('client connection success', done => {
    let clientOnReady = jest.fn(() => {
      done();
      client.disconnect();
    });

    client = new Client(addressA, "dummy");
    client.onConnect = clientOnReady;
    client.connect();
  });

  test('client connection failure - invalid address', () => {
    let clientOnReady = jest.fn();

    client = new Client("invalid");
    client.onConnect = clientOnReady;

    try {
      client.connect();
    } catch (e) {
      expect(e).toEqual(new Error("Failed to connect to client - invalid address \"invalid\""));
      expect(clientOnReady).not.toHaveBeenCalled();
    }
  });

  test('client connection failure - no server', () => {
    let clientOnReady = jest.fn();

    client = new Client(addressB);
    client.onConnect = clientOnReady;
    client.connect();

    setTimeout(() => {
      expect(clientOnReady).not.toHaveBeenCalled();
    }, 100);
  });
});


describe('robot get commands', () => {
  var client;
  var server;

  beforeEach(() => {
    server = new Server(addressA);
    server.setup(stage);
    client = new Client(addressA);
  })

  afterEach(() => {
    client.disconnect();
    server.close();
    server = null;
  });

  test('list robots', done => {
    expect.assertions(1);

    let clientOnConnect = jest.fn(async () => {
      let robots = await client.listRobots();
      expect(server.listRobots).toHaveBeenCalled();
      done();
    })
    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get robot actual pose', done => {
    // expect.assertions(4);

    let clientOnConnect = jest.fn(async () => {
      let robot0ActualPose = await client.getActualPose(0);
      let robot5ActualPose = await client.getActualPose(5);
      try {
        let robotXActualPose = await client.getActualPose(99);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getActualPose).toHaveBeenCalledTimes(3);
      expect(robot0ActualPose.payload).toEqual(stage.robots[0].poseActual);
      expect(robot5ActualPose.payload).toEqual(stage.robots[5].poseActual);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get robot joint count', done => {
    // expect.assertions(4);

    let clientOnConnect = jest.fn(async () => {
      let robot0JointCount = await client.getJointCount(0);
      let robot5JointCount = await client.getJointCount(5);
      try {
        let robotXJointCount = await client.getJointCount(99);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointCount).toHaveBeenCalledTimes(3);
      expect(robot0JointCount.payload).toEqual(3);
      expect(robot5JointCount.payload).toEqual(1);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get robot position state', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0PositionState = await client.getActualPositionState(0);
      let robot5PositionState = await client.getActualPositionState(5);
      try {
        let robotXPositionState = await client.getActualPositionState(99);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getActualPositionState).toHaveBeenCalledTimes(3);
      expect(robot0PositionState.payload).toEqual(stage.robots[0].positionStateActual);
      expect(robot5PositionState.payload).toEqual(stage.robots[5].positionStateActual);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });
});


describe('robot set commands', () => {
  var client;
  var server;
  let count = 0;

  beforeEach(() => {
    server = new Server(addressA);
    server.setup(stage);
    client = new Client(addressA);
  })

  afterEach(() => {
    client.disconnect();
    count++;
    server.close();
    server = null;
  });

  test('set robot target position state', done => {
    let clientOnConnect = jest.fn(async () => {
      await client.setTargetPositionState(0, testPositionState);
      await client.setTargetPositionState(5, [999]);
      try {
        let robotXPositionState = await client.setTargetPositionState(99, [100]);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.setTargetPositionState).toHaveBeenCalledTimes(3);
      expect(server.stage.robots[0].positionStateTarget).toEqual(testPositionState);
      expect(server.stage.robots[5].positionStateTarget).toEqual([999]);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('set robot target pose', done => {
    // expect.assertions(4);

    let clientOnConnect = jest.fn(async () => {
      await client.setTargetPose(0, testPose);
      let robot0TargetPose = await client.getTargetPose(0);
      try {
         await client.setTargetPose(99, testPose);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.setTargetPose).toHaveBeenCalledTimes(2);
      expect(server.stage.robots[0].poseTarget).toEqual(testPose);
      expect(robot0TargetPose.payload).toEqual(testPose);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

});

describe('joint get commands', () => {
  var client;
  var server;
  let count = 0;

  beforeEach(() => {
    server = new Server(addressA);
    server.setup(stage);
    let cname = "client" + count.toString();
    client = new Client(addressA);
  })

  afterEach(() => {
    client.disconnect();
    count++;
    server.close();
    server = null;
  });

  test('get joint actuals (position)', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Actual = await client.getJointActual(0, 0, ServoControlMode.POSITION);
      let robot5Joint0Actual = await client.getJointActual(5, 0, ServoControlMode.POSITION);
      try {
        let robotXJoint0Actual = await client.getJointActual(99, 0, ServoControlMode.POSITION);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXActual = await client.getJointActual(0, 99, ServoControlMode.POSITION);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointActual).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Actual.payload).toEqual(stage.robots[0].servos[0].positionActual);
      expect(robot5Joint0Actual.payload).toEqual(stage.robots[5].servos[0].positionActual);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint actuals (velocity)', done => {
    // expect.assertions(5);

    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Actual = await client.getJointActual(0, 0, ServoControlMode.VELOCITY);
      let robot5Joint0Actual = await client.getJointActual(5, 0, ServoControlMode.VELOCITY);
      try {
        let robotXJoint0Actual = await client.getJointActual(99, 0, ServoControlMode.VELOCITY);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXActual = await client.getJointActual(0, 99, ServoControlMode.VELOCITY);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointActual).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Actual.payload).toEqual(stage.robots[0].servos[0].velocityActual);
      expect(robot5Joint0Actual.payload).toEqual(stage.robots[5].servos[0].velocityActual);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint actuals (torque)', done => {
    // expect.assertions(5);

    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Actual = await client.getJointActual(0, 0, ServoControlMode.TORQUE);
      let robot5Joint0Actual = await client.getJointActual(5, 0, ServoControlMode.TORQUE);
      try {
        let robotXJoint0Actual = await client.getJointActual(99, 0, ServoControlMode.TORQUE);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXActual = await client.getJointActual(0, 99, ServoControlMode.TORQUE);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointActual).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Actual.payload).toEqual(stage.robots[0].servos[0].torqueActual);
      expect(robot5Joint0Actual.payload).toEqual(stage.robots[5].servos[0].torqueActual);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint brake status', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Brake = await client.getJointBrake(0, 0);
      let robot5Joint0Brake = await client.getJointBrake(5, 0);
      try {
        let robotXJoint0Brake = await client.getJointBrake(99, 0);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXBrake = await client.getJointBrake(0, 99);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointBrake).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Brake.payload).toEqual(stage.robots[0].servos[0].brake);
      expect(robot5Joint0Brake.payload).toEqual(stage.robots[5].servos[0].brake);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint control mode', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0ControlMode = await client.getJointControlMode(0, 0);
      let robot5Joint0ControlMode = await client.getJointControlMode(5, 0);
      try {
        let robotXJoint0ControlMode = await client.getJointControlMode(99, 0);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXControlMode = await client.getJointControlMode(0, 99);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointControlMode).toHaveBeenCalledTimes(4);
      expect(robot0Joint0ControlMode.payload).toEqual(stage.robots[0].servos[0].controlMode);
      expect(robot5Joint0ControlMode.payload).toEqual(stage.robots[5].servos[0].controlMode);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint targets (position)', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Target = await client.getJointTarget(0, 0, ServoControlMode.POSITION);
      let robot5Joint0Target = await client.getJointTarget(5, 0, ServoControlMode.POSITION);
      try {
        let robotXJoint0Target = await client.getJointTarget(99, 0, ServoControlMode.POSITION);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXTarget = await client.getJointTarget(0, 99, ServoControlMode.POSITION);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointTarget).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Target.payload).toEqual(stage.robots[0].servos[0].positionTarget);
      expect(robot5Joint0Target.payload).toEqual(stage.robots[5].servos[0].positionTarget);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint targets (velocity)', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Target = await client.getJointTarget(0, 0, ServoControlMode.VELOCITY);
      let robot5Joint0Target = await client.getJointTarget(5, 0, ServoControlMode.VELOCITY);
      try {
        let robotXJoint0Target = await client.getJointTarget(99, 0, ServoControlMode.VELOCITY);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXTarget = await client.getJointTarget(0, 99, ServoControlMode.VELOCITY);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointTarget).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Target.payload).toEqual(stage.robots[0].servos[0].velocityTarget);
      expect(robot5Joint0Target.payload).toEqual(stage.robots[5].servos[0].velocityTarget);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });

  test('get joint targets (torque)', done => {
    let clientOnConnect = jest.fn(async () => {
      let robot0Joint0Target = await client.getJointTarget(0, 0, ServoControlMode.TORQUE);
      let robot5Joint0Target = await client.getJointTarget(5, 0, ServoControlMode.TORQUE);
      try {
        let robotXJoint0Target = await client.getJointTarget(99, 0, ServoControlMode.TORQUE);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      try {
        let robot0JointXTarget = await client.getJointTarget(0, 99, ServoControlMode.TORQUE);
      } catch (e) {
        expect(e.payload.toString()).toMatch('Error: Command');
      }
      expect(server.getJointTarget).toHaveBeenCalledTimes(4);
      expect(robot0Joint0Target.payload).toEqual(stage.robots[0].servos[0].torqueTarget);
      expect(robot5Joint0Target.payload).toEqual(stage.robots[5].servos[0].torqueTarget);
      done();
    });

    client.onConnect = clientOnConnect;
    client.connect();
  });
});
