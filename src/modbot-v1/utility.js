export function messageFromFloatArray(arr, size = 8) {
  let msg = new Message();
  msg.addUint(arr.length);
  msg.addBuffer(floatArrayToBytes(arr, size));
  return msg;
}

export function messageFromIntArray(arr, size = 4) {
  let msg = new Message();
  msg.addUint(arr.length);
  msg.addBuffer(intArrayToBytes(arr, size));
  return msg;
}

export function messageFromPose(pose) {
  let msg = new Message();
  msg.addBuffer(floatArrayToBytes(pose.x, pose.y, pose.z));
  msg.addBuffer(floatArrayToBytes(pose.qw, pose.qx, pose.qy, pose.qz));
  return msg;
}

export function messageToFloatArray(message, size = 8) {
  let arr = [];
  let count = message.popUint();
  for (let i = 0; i < count; i++) {
    arr.push(message.popFloat(size));
  }
  return arr;
}

export function messageToIntArray(message, size = 4) {
  let arr = [];
  let count = message.popUint();
  for (let i = 0; i < count; i++) {
    arr.push(message.popInt(size));
  }
  return arr;
}

export function messageToPose(message) {
  return {
    x: message.popFloat(),
    y: message.popFloat(),
    z: message.popFloat(),
    qw: message.popFloat(),
    qx: message.popFloat(),
    qy: message.popFloat(),
    qz: message.popFloat(),
  };
}