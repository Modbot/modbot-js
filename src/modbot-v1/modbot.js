import { Dealer, Message, NumberUtility } from '@modbot/jsmq'
import * as Command from './commands.js'
import {
  messageFromFloatArray,
  messageFromIntArray,
  messageFromPose,
  messageToFloatArray,
  messageToIntArray,
  messageToPose
} from './utility.js'


NumberUtility.littleEndian = true;

/**
 @typedef Pose
 @type {Object}
 @property {number} x   - The x coordinate in meters
 @property {number} y   - The y coordinate in meters
 @property {number} z   - The z coordinate in meters
 @property {number} qw  - The qw quaternion value
 @property {number} qx  - The qx quaternion value
 @property {number} qy  - The qy quaternion value
 @property {number} qz  - The qy quaternion value
 */

/**
 @typedef Response
 @type {Object}
 @property {Status} status  - The return status of the response
 @property {Object} payload - The payload of the response
 */


/**
 * Modbot API call return statuses
 */
export const Status = Object.freeze({
  SUCCESS:                0,
  SEND_FAILED:           -1,
  RECV_FAILED:           -2,
  COMMAND_FAILED:        -3,
  INVALID_ARGUMENT:      -4,
});

/**
 * Modbot servo control modes
 */
export const ServoControlMode = Object.freeze({
  NONE:                   0,
  POSITION:               1,
  VELOCITY:               2,
  TORQUE:                 3,
});


/**
 * Class representing a WebSocket endpoint
 * @param {string} address - The endpoint address (ex: "ws://127.0.0.1:15798")
 */
export class Client {
  constructor(address) {
    console.log("ModbotJS | Client created with address", address);
    this._address = address;
    this._dealer = null;
    this._dealer = new Dealer();
    this.onConnect = null;
    this.onDisconnect = null;
  }

  _createJointRequest(robotId, jointId) {
    let msg = new Message();
    msg.addUint(robotId, 2);
    msg.addUint(jointId, 2);
    return msg;
  }

  _createRobotRequest(id) {
    let msg = new Message();
    msg.addUint(id, 2);
    return msg;
  }

  _onConnect() {
    console.log("ModbotJS | Client connected to " + this._address);
    if (this.onConnect !== null) {
      this.onConnect();
    }
  }

  _onDisconnect() {
    console.log("ModbotJS | Client disconnected from " + this._address);
    if (this.onDisconnect !== null) {
      this.onDisconnect();
    }
  }

  /**
   * Sends a command to the host
   *
   * @param {string} service  - The command service
   * @param {Command} command - The command 2 bit integer
   * @param {Message} message - The command message, containing any additional parameters for the command
   * @return {Response}       - The response
   */
  _sendCommand(service, command, message) {
    if (message === undefined) {
      message = new Message;
    }
    message.insertString(0, service);
    message.insertInt(1, command, 2);

    let rc = this._dealer.send(message);
    if (!rc) {
      return Promise.reject(new Error("Failed to send request"));
    }

    return this._dealer.receive().then( message => {
      // The first frame should be the command that was sent (int 16)
      //  If it isn't, it is an int 32 error code, followed by a string reason
      let commandRes = message.getInt(0, 2);
      if (commandRes !== command) {
        let code = message.popInt();
        let reason = message.popString();
        let str = "Command " + command + " failed with error code " + code  + ", reason \"" + reason + "\"";
        console.log(str);
        return Promise.reject( {
          status: Status.COMMAND_FAILED,
          payload: new Error(str),
        });

      } else {
        message.popInt(2);  // discard command
        return {
          status: Status.SUCCESS,
          payload: message,
        };
      }
    }, error => {
      return {
        status: Status.RECV_FAILED,
        payload: error,
      };
    });
  }

  _sendIOCommand(command, masterId, deviceId, message = undefined) {
    if (message === undefined) {
      message = new Message();
    }
    message.insertUint(0, masterId);
    message.insertUint(1, deviceId);
    return this._sendCommand("io", command, message);
  }

  _sendJointCommand(command, robotId, jointId, message = undefined) {
    if (message === undefined) {
      message = new Message();
    }
    message.insertUint(0, robotId);
    message.insertUint(1, jointId);
    return this._sendMotionCommand(command, message);
  }

  _sendMotionCommand(command, message) {
    return this._sendCommand("motion", command, message);
  }

  _sendRobotCommand(command, robotId, message = undefined) {
    if (message === undefined) {
      message = new Message();
    }
    message.insertUint(0, robotId);
    return this._sendMotionCommand(command, message);
  }

  _sendSequenceCommand(command, message = undefined) {
    return this._sendCommand("sequencer", command, message);
  }

  // -------------------------- client connection  -------------------------- //

  /**
   * Connects to the host
   */
  connect() {
    if (this._address.length < 5 ||
        ((this._address.substring(0, 5) !== "ws://" ) &&
        (this._address.substring(0, 6) !== "wss://"))) {
      throw new Error("Failed to connect to client - invalid address \"" + this._address + "\"");
    }

    console.log("ModbotJS | Client connecting to " + this._address + "...");
    this._dealer.sendReady = this._onConnect.bind(this);
    this._dealer.onDisconnect = this._onDisconnect.bind(this);
    this._dealer.connect(this._address);
  }

  /**
   * Disconnects from the host
   *
   * @param {number} code     - Disconnect code (default 1000)
   * @param {string} reason   - Disconnect reason (default "Connection close requested")
   */
  disconnect(code = 1000, reason = "Connection close requested") {
    console.log("ModbotJS | Client disconnecting...");
    this._dealer.onDisconnect = this._onDisconnect.bind(this);
    try {
      this._dealer.disconnect(this._address, code, reason);
    } catch (e) {
      console.log(e.message);
    }
  }


  ///////////////////////////////// API CALLS //////////////////////////////////


  // --------------------------------- get  --------------------------------- //

  /**
   * Gets the actual pose of a robot
   *
   * @param {number} robotId - The ID of the robot
   * @return {Promise<Response<Pose>>} - The actual pose of the robot
   */
  getActualPose(robotId) {
    let res = this._sendRobotCommand(Command.GET_ACTUAL_POSE, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let pose = messageToPose(response.payload);
      return {
        status: response.status,
        payload: pose,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }
  
  /**
   * Gets the actual position state of a robot
   *
   * @param {number} robotId  - The ID of the robot
   * @return {Promise<Response<number[]>>} - The call status and payload;
   *    the actual position state of the robot as an array of joint actuals in radians
   */
  getActualPositionState(robotId) {
    let res = this._sendRobotCommand(Command.GET_ACTUAL_POSITION_STATE, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = [];
      let count = response.payload.popUint();
      for (let i = 0; i < count; i++) {
        state.push(response.payload.popFloat());
      }
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the analog inputs of an I/O device
   *
   * @param {number} masterId - The master ID of the I/O device
   * @param {number} deviceId - The (slave) ID of the I/O device
   * @return {Promise<Response<{ id: string, value: number }[]>>} - The call status and payload;
   *    an array of analog inputs
   */
  getAnalogInputs(masterId, deviceId) {
    let res = this._sendIOCommand(Command.GET_ANALOG_INPUTS, masterId, deviceId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let io = [];
      const count = response.payload.popUint();
      for (var i = 0; i < count; i++) {
        io.push({
          id: response.payload.popString(),
          value: response.payload.popUint(),
        });
      }
      return {
        status: response.status,
        payload: io,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the digital inputs of an I/O device
   *
   * @param {number} masterId - The master ID of the I/O device
   * @param {number} deviceId - The (slave) ID of the I/O device
   * @return {Promise<Response<{ id: string, value: number }[]>>} - The call status and payload;
   *    an array of digital inputs
   */
  getDigitalInputs(masterId, deviceId) {
    let res = this._sendIOCommand(Command.GET_DIGITAL_INPUTS, masterId, deviceId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let io = [];
      const count = response.payload.popUint();
      for (var i = 0; i < count; i++) {
        io.push({
          id: response.payload.popString(),
          value: response.payload.popBoolean(),
        });
      }
      return {
        status: response.status,
        payload: io,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the actual value of a joint, based on the provided control mode
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {ServoControlMode} controlMode  - The control mode of the desired actual value
   * @return {Promise<Response<number>>} - The call status and payload; the joint actual
   */
  getJointActual(robotId, jointId, controlMode) {
    let req = new Message().addInt(controlMode);
    let res = this._sendJointCommand(Command.GET_JOINT_ACTUAL, robotId, jointId, req);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let actual = response.payload.popFloat();
      return {
        status: response.status,
        payload: actual,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the analog inputs of a joint
   *
   * @param {number} robotId  - The ID of the robot
   * @param {number} jointId  - The ID of the joint
   * @return {Promise<Response<{ id: string, value: number }[]>>} - The call status and payload;
   *    an array of analog inputs
   */
  getJointAnalogInputs(robotId, jointId) {
    let res = this._sendJointCommand(Command.GET_JOINT_ANALOG_INPUTS, robotId, jointId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let io = [];
      const count = response.payload.popUint();
      for (var i = 0; i < count; i++) {
        io.push({
          id: response.payload.popString(),
          value: response.payload.popUint(2),
        });
      }
      return {
        status: response.status,
        payload: io,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the brake state of a joint
   *
   * @param {number} robotId - The ID of the robot
   * @param {number} jointId - The ID of the joint
   * @return {Promise<Response<Boolean>>} - The call status and payload;
   *    state of the brake (false: disengaged; true: engaged)
   */
  getJointBrake(robotId, jointId) {
    let res = this._sendJointCommand(Command.GET_JOINT_BRAKE, robotId, jointId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = response.payload.popBoolean();
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the joint count of a robot
   *
   * @param {number} robotId  - The ID of the robot
   * @return {Promise<Response<number>>} - The call status and payload; the joint count
   */
  getJointCount(robotId) {
    let res = this._sendRobotCommand(Command.GET_JOINT_COUNT, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let count = response.payload.popUint();
      return {
        status: response.status,
        payload: count,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the control mode of a joint
   *
   * @param {number} robotId    - The ID of the robot
   * @param {number} jointId    - The ID of the joint
   * @return {Promise<Response<ServoControlMode>>} - The call status and payload;
   *    the control mode of the joint
   */
  getJointControlMode(robotId, jointId) {
    let res = this._sendJointCommand(Command.GET_JOINT_CONTROL_MODE, robotId, jointId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = response.payload.popInt();
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the digital inputs of a joint
   *
   * @param {number} robotId  - The ID of the robot
   * @param {number} jointId  - The ID of the joint
   * @return {Promise<Response<{ id: string, value: Boolean }[]>>} - The call status and payload;
   *    an array of digital inputs
   */
  getJointDigitalInputs(robotId, jointId) {
    let res = this._sendJointCommand(Command.GET_JOINT_DIGITAL_INPUTS, robotId, jointId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let io = [];
      const count = response.payload.popUint();
      for (var i = 0; i < count; i++) {
        io.push({
          id: response.payload.popString(),
          value: response.payload.popBoolean(),
        });
      }
      return {
        status: response.status,
        payload: io,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the digital outputs of a joint
   *
   * @param {number} robotId  - The ID of the robot
   * @param {number} jointId  - The ID of the joint
   * @return {Promise<Response<{ id: string, value: Boolean }[]>>} - The call status and payload;
   *    an array of digital outputs
   */
  getJointDigitalOutputs(robotId, jointId) {
    let res = this._sendJointCommand(Command.GET_JOINT_DIGITAL_OUTPUTS, robotId, jointId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let io = [];
      const count = response.payload.popUint();
      for (var i = 0; i < count; i++) {
        io.push({
          id: response.payload.popString(),
          value: response.payload.popBoolean(),
        });
      }
      return {
        status: response.status,
        payload: io,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets a the hardware parameter of a joint
   *
   * @param {number} robotId    - The ID of the robot
   * @param {number} jointId    - The ID of the joint
   * @param {string} parameter  - The name of the parameter to fetch
   * @return {Promise<Response<number>>} - The call status and payload;
   *    the value of the fetched parameter
   */
  getJointParameter(robotId, jointId, parameter) {
    let req = new Message().addString(parameter);
    let res = this._sendJointCommand(Command.GET_JOINT_PARAMETER, robotId, jointId, req);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let value = response.payload.popFloat();
      return {
        status: response.status,
        payload: value,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the target of a joint
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {ServoControlMode} controlMode  - The control mode of the target
   * @return {Promise<Response<number>>}    - The call status and payload; the target of the joint
   */
  getJointTarget(robotId, jointId, controlMode) {
    let req = new Message().addInt(controlMode);
    let res = this._sendJointCommand(Command.GET_JOINT_TARGET, robotId, jointId, req);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let target = response.payload.popFloat();
      return {
        status: response.status,
        payload: target,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the control mode of a robot
   *
   * The robot's control mode is the combined control mode of all of the robot's joints;
   *  If the joints are in multiple different control modes, the robot's control mode is NONE.
   *
   * @param {number} robotId  - The ID of the robot
   * @return {Promise<Response<ServoControlMode>>} - The call status and payload;
   *    the control mode of the robot
   */
  getRobotControlMode(robotId) {
    let res = this._sendRobotCommand(Command.GET_ROBOT_CONTROL_MODE, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = response.payload.popInt();
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the run status of a robot
   *
   * @param {number} robotId  - The ID of the robot
   * @param {number} jointId  - The ID of the joint
   * @return {Promise<Response<Boolean>>} - The call status and payload;
   *    the run status of the robot (false: stop; true: run)
   */
  getRobotRun(robotId) {
    let res = this._sendRobotCommand(Command.GET_ROBOT_RUN, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = response.payload.popBoolean();
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Reports whether a sequence is currently running.
   */
  getSequenceIsRunning() {
    const res = this._sendSequenceCommand(Command.IS_RUNNING);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);
 
      // Call succeeded
      return {
        status: response.status, 
        payload: response.payload.popBoolean(),
      }
    }, (error) => {
      return Promise.reject(error);
    })
  }

  /**
   * Gets the status of the sequencer
   *   0 - unknown
   *   1 - stopped
   *   2 - running
   *   3 - completed
   *   4 - failed
   *   5 - timeout
   */
  getSequencerStatus() {
    const res = this._sendSequenceCommand(Command.SEQUENCE_STATUS);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);
 
      // Call succeeded
      return {
        status: response.status, 
        payload: response.payload.popInt(),
      }
    }, (error) => {
      return Promise.reject(error);
    })
  }

  /**
   * Gets the target pose of a robot
   *
   * @param {number} robotId  - The ID of the robot
   * @return {Promise<Response<Pose>>} - The call status and payload; the target pose of the robot
   */
  getTargetPose(robotId) {
    let res = this._sendRobotCommand(Command.GET_TARGET_POSE, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let pose = messageToPose(response.payload);
      return {
        status: response.status,
        payload: pose,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the target position state of a robot
   *
   * @param {number} robotId  - The ID of the robot
   * @return {Promise<Response<number[]>>} - The call status and payload;
   *    the target position state of the robot as an array of joint targets in radians
   */
  getTargetPositionState(robotId) {
    let res = this._sendRobotCommand(Command.GET_TARGET_POSITION_STATE, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = [];
      let count = response.payload.popUint();
      for (let i = 0; i < count; i++) {
        state.push(response.payload.popFloat());
      }
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the velocity state of a robot
   *
   * @param {number} robotId  - The ID of the robot
   * @return {Promise<Response<number[]>>} - The call status and payload;
   *    the velocity state as an array of joint velocities in radians / second
   */
  getTargetVelocityState(robotId) {
    let res = this._sendRobotCommand(Command.GET_TARGET_VELOCITY_STATE, robotId);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let state = messageToFloatArray(response.payload);
      return {
        status: response.status,
        payload: state,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Halts a running sequence, if a sequence is running
   */
  haltSequence() {
    return this._sendSequenceCommand(Command.HALT_SEQUENCE);
  }

  /**
   * Gets the available I/O devices
   *
   * @return {Promise<Response<{ masterId: number, deviceId: number }[]>>} - The call status and payload; an array of robots
   */
  listIODevices() {
    let res = this._sendCommand("io", Command.LIST_IO_DEVICES);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let devices = [];
      let count = response.payload.popUint();
      for (let i = 0; i < count; i++) {
        devices.push({
          masterId: response.payload.popUint(),
          deviceId: response.payload.popUint(),
          description: response.payload.popString(),
          type: response.payload.popInt(),
        });
      }
      return {
        status: response.status,
        payload: devices,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Gets the available robots
   *
   * @return {Promise<Response<{ id: number, name: string }[]>>} - The call status and payload; an array of robots
   */
  listRobots() {
    let res = this._sendCommand("discovery", Command.LIST_ROBOTS);
    return res.then((response) => {
      // Call failed
      if (response.status < Status.SUCCESS)
        return Promise.reject(response);

      // Call succeeded
      let robots = {}
      let count = response.payload.popUint();
      for (let i = 0; i < count; i++) {
        robots[i] = {
          id: response.payload.popUint(),
          name: response.payload.popString()
        };
      }
      return {
        status: response.status,
        payload: robots,
      };

    // Call failed
    }, (error) => {
      return Promise.reject(error);
    });
  }

  /**
   * Runs or stops a robot
   *
   * @param {number} robotId              - The ID of the robot
   * @param {Boolean} run                 - The desired run state of the robot (false: stop; true: run)
   * @return {Promise<Response<Boolean>>} - The call status and payload; the run state of the robot (false: stop; true: run)
   */
  setRobotRun(robotId, run) {
    let state = (run ? 1 : 0);
    let req = new Message().addInt(state, 1);
    return this._sendRobotCommand(Command.SET_ROBOT_RUN, robotId, req);
  }

  /**
   * Runs a string sequence
   * 
   * @param {String} sequence 
   */
  runSequence(sequence) {
    let req = new Message().addString(sequence);
    return this._sendSequenceCommand(Command.RUN_SEQUENCE, req);
  }

  /**
   * Saves a sequence to file
   * 
   * @param {String} filename 
   * @param {String} sequence 
   */
  saveSequenceFile(filename, sequence) {
    let req = new Message();
    req.addString(filename);
    req.addString(sequence);
    return this._sendSequenceCommand(Command.SEQUENCE_FILE, req);
  }


  // --------------------------------- set  --------------------------------- //

  /**
   * Sets the digital outputs of an I/O device
   *
   * @param {number} masterId                                 - The master ID of the I/O device
   * @param {number} deviceId                                 - The (slave) ID of the I/O device
   * @param {{ id: string, value: Boolean }[]} digitalIOList  - The array of target digital outputs
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setDigitalOutputs(masterId, deviceId, digitalIOList) {
    let req = new Message();
    req.addUint(digitalIOList.length);
    for (var i = 0; i < digitalIOList.length; i++) {
      req.addString(digitalIOList[i].id);
      req.addBoolean(digitalIOList[i].value);
    }
    return this._sendIOCommand(Command.SET_DIGITAL_OUTPUTS, masterId, deviceId, req);
  }

  /**
   * Sets the brake state of a joint
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {Boolean} brake                 - The desired brake state of the joint (false: disengaged; true: engaged)
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setJointBrake(robotId, jointId, brake) {
    let req = new Message().addBoolean(brake);
    return this._sendJointCommand(Command.SET_JOINT_BRAKE, robotId, jointId, req);
  }

  /**
   * Sets the control mode of a joint
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {ServoControlMode} controlMode  - The desired control mode of the joint
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setJointControlMode(robotId, jointId, controlMode) {
    let req = new Message().addInt(controlMode);
    return this._sendJointCommand(Command.SET_JOINT_CONTROL_MODE, robotId, jointId, req);
  }

  /**
   * Sets the digital outputs of a joint
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {{ id: string, value: Boolean }[]} digitalIOList        - The array of target digital outputs
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setJointDigitalOutputs(robotId, jointId, digitalIOList) {
    let req = new Message();
    req.addUint(digitalIOList.length);
    for (var i = 0; i < digitalIOList.length; i++) {
      req.addString(digitalIOList[i].id);
      req.addBoolean(digitalIOList[i].value);
    }
    return this._sendJointCommand(Command.SET_JOINT_DIGITAL_OUTPUTS, robotId, jointId, req);
  }

  /**
   * Sets the brake state of a joint
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {string} parameter              - The parameter name
   * @param {number} value                  - The parameter target value
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setJointParameter(robotId, jointId, parameter, value) {
    let req = new Message().addString(parameter).addFloat(value);
    return this._sendJointCommand(Command.SET_JOINT_PARAMETER, robotId, jointId, req);
  }

  /**
   * Sets the target of a joint, based on the specified control mode
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number} jointId                - The ID of the joint
   * @param {ServoControlMode} controlMode  - The target control mode
   * @param {number} target                 - The target
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setJointTarget(robotId, jointId, controlMode, target) {
    let req = new Message().addInt(controlMode).addFloat(target);
    return this._sendJointCommand(Command.SET_JOINT_TARGET, robotId, jointId, req);
  }

  
  /**
   * Sets the relative target pose of a robot
   *
   * @param {number} robotId                - The ID of the robot
   * @param {Pose} pose                     - The desired pose of the robot
   * @return {Promise.<Object, Object>}     - The call status and payload
   */
  setRelativeTargetPose(robotId, pose) {
    let req = new Message();
    req.addBuffer(NumberUtility.floatArrayToBytes([pose.x, pose.y, pose.z], 8));
    req.addBuffer(NumberUtility.floatArrayToBytes([pose.qw, pose.qx, pose.qy, pose.qz], 8));
    return this._sendRobotCommand(Command.SET_RELATIVE_TARGET_POSE, robotId, req);
  }

  /**
   * Sets the relative target position state of a robot
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number[]} state                - The target position state of the robot
   * @param {number} velocity               - The target velocity for the coordinated joint movement
   * @return {Promise.<Object, Object>}     - The call status and payload
   */
  setRelativeTargetPositionState(robotId, state, velocity) {
    let req = new Message();
    req.addUint(state.length);
    req.addBuffer(NumberUtility.floatArrayToBytes(state, 8));
    req.addFloat(velocity);
    return this._sendRobotCommand(Command.SET_RELATIVE_TARGET_POSITION_STATE, robotId, req);
  }

  /**
   * Sets the control mode of a robot
   *
   * @param {number} robotId                - The ID of the robot
   * @param {ServoControlMode} controlMode  - The desired control mode
   * @return {Promise.<Response<Object>>}   - The call status and payload
   */
  setRobotControlMode(robotId, controlMode) {
    let req = new Message().addInt(controlMode);
    return this._sendRobotCommand(Command.SET_ROBOT_CONTROL_MODE, robotId, req);
  }

  /**
   * Sets the target pose of a robot
   *
   * @param {number} robotId                - The ID of the robot
   * @param {Pose} pose                     - The desired pose of the robot
   * @return {Promise.<Object, Object>}     - The call status and payload
   */
  setTargetPose(robotId, pose) {
    let req = new Message();
    req.addBuffer(NumberUtility.floatArrayToBytes([pose.x, pose.y, pose.z], 8));
    req.addBuffer(NumberUtility.floatArrayToBytes([pose.qw, pose.qx, pose.qy, pose.qz], 8));
    return this._sendRobotCommand(Command.SET_TARGET_POSE, robotId, req);
  }
  
  /**
   * Sets the target position state of a robot
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number[]} state                - The target position state of the robot
   * @param {number} velocity               - The target velocity for the coordinated joint movement
   * @return {Promise.<Object, Object>}     - The call status and payload
   */
  setTargetPositionState(robotId, state, velocity) {
    let req = new Message();
    req.addUint(state.length);
    req.addBuffer(NumberUtility.floatArrayToBytes(state, 8));
    req.addFloat(velocity);
    return this._sendRobotCommand(Command.SET_TARGET_POSITION_STATE, robotId, req);
  }

  /**
   * Sets the target velocity state of a robot
   *
   * @param {number} robotId                - The ID of the robot
   * @param {number[]} state                - The target velocity state of the robot
   * @return {Promise.<Object, Object>}     - The call status and payload
   */
  setTargetVelocityState(robotId, state) {
    let req = messageFromFloatArray(state);
    return this._sendRobotCommand(Command.SET_TARGET_VELOCITY_STATE, robotId, req);
  }
}