// Motion commands
//  Robot commands
export const SET_ROBOT_RUN                                  = 1001;
export const GET_JOINT_COUNT                                = 1002;
export const GET_ACTUAL_POSE                                = 1003;
export const GET_TARGET_POSE                                = 1004;
export const SET_TARGET_POSE                                = 1005;
export const GET_ACTUAL_POSITION_STATE                      = 1006;
export const SET_TARGET_POSITION_STATE                      = 1007;
export const GET_TARGET_VELOCITY_STATE                      = 1008;
export const SET_TARGET_VELOCITY_STATE                      = 1009;
export const GET_ROBOT_RUN                                  = 1010;
export const GET_ROBOT_CONTROL_MODE                         = 1011;
export const SET_ROBOT_CONTROL_MODE                         = 1012;

export const SET_RELATIVE_TARGET_POSE                       = 1013;
export const SET_RELATIVE_TARGET_POSITION_STATE             = 1014;
export const GET_TARGET_POSITION_STATE                      = 1015;
export const GET_POSITION_STREAM                            = 2001;

//  Joint commands
export const GET_JOINT_CONTROL_MODE                         = 10001;
export const SET_JOINT_CONTROL_MODE                         = 10002;
export const GET_JOINT_BRAKE                                = 10003;
export const SET_JOINT_BRAKE                                = 10004;
export const GET_JOINT_ACTUAL                               = 10005;
export const GET_JOINT_TARGET                               = 10006;
export const SET_JOINT_TARGET                               = 10007;
export const GET_JOINT_PARAMETER                            = 10101;
export const SET_JOINT_PARAMETER                            = 10102;
export const GET_JOINT_ANALOG_INPUTS                        = 10011;
export const GET_JOINT_DIGITAL_INPUTS                       = 10021;
export const GET_JOINT_DIGITAL_OUTPUTS                      = 10031;
export const SET_JOINT_DIGITAL_OUTPUTS                      = 10032;

// Discovery commands
export const LIST_SERVICES                                  = 1;
export const LIST_ROBOTS                                    = 2;
export const GET_HOSTNAME                                   = 1001;
export const GET_ROBOT_COUNT                                = 1002;

// I/O commands
export const LIST_IO_DEVICES                                = 1;
export const GET_ANALOG_INPUTS                              = 1001;
export const GET_DIGITAL_INPUTS                             = 2001;
export const GET_DIGITAL_OUTPUTS                            = 2002;
export const SET_DIGITAL_OUTPUTS                            = 2003;

// Sequence commands
export const RUN_SEQUENCE                                   = 1;
export const HALT_SEQUENCE                                  = 2;
export const IS_RUNNING                                     = 3;
export const SEQUENCE_FILE                                  = 4;
export const SEQUENCE_STATUS                                = 5;